# University of Amsterdam Chat Application

## Client Login
- Download de chat.zip file
- Extract de file
- Open a terminal window
- Navigate to the chat/ directory
- execute the underneath command:

  ./connect

### Start server
python server.py

### Start Client
python client.py localhost 5000

### Run server permanently (NO Hang UP)
nohup python server.py

### Kill PID
ps -ef | grep python || nohup && kill -9 $PID

### Cloud start client
ssh -i "uva.pem" ubuntu@52.28.55.203 -t 'python /home/ubuntu/chatapp/client.py localhost 5000'
