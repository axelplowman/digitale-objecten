#!/usr/bin/python
import socket, select


def broadcast_data(sock, message):
    for socket in CONNECTION_LIST:
        if socket != server_socket and socket != sock:
            try:
                socket.send(message)
            except:
                socket.close()
                CONNECTION_LIST.remove(socket)


if __name__ == "__main__":
    CONNECTION_LIST = []
    RECV_BUFFER = 4096
    PORT = 5000

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(("0.0.0.0", PORT))
    server_socket.listen(10)

    CONNECTION_LIST.append(server_socket)
    print "Chat server is gestart op poort nummer: " + str(PORT)

    while 1:
        read_sockets, write_sockets, error_sockets = select.select(CONNECTION_LIST, [], [])

        for sock in read_sockets:

            if sock == server_socket:  # Nieuwe connectie
                s, addr = server_socket.accept()
                CONNECTION_LIST.append(s)
                print "Nieuwe client is verbonden op poort %s\n" % addr[1]

                broadcast_data(s, "Nieuwe client is in de chat ruimte op poort: %s \n" % addr[1])

            else:  # Anders is er een inkomende boodschap

                try:
                    data = sock.recv(RECV_BUFFER)
                    if data:
                        broadcast_data(sock, data)
                except:
                    broadcast_data(sock, "Client op poort: %s is offline" % addr[1])
                    print "Client op poort: %s is nu offline" % addr[1]
                    sock.close()
                    CONNECTION_LIST.remove(sock)
                    continue

    server_socket.close()
