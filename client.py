#!/usr/bin/python
import socket, select, sys

user_name = raw_input('\nVoer U naam in: ')

print """
                                       ,---,
                 ,--,         ,---.   '  .' \
               ,'_ /|        /__./|  /  ;    '.
          .--. |  | :   ,---.;  ; | :  :       \
        ,'_ /| :  . |  /___/ \  | | :  |   /\   \
        |  ' | |  . .  \   ;  \ ' | |  :  ' ;.   :
        |  | ' |  | |   \   \  \: | |  |  ;/  \   \
        :  | | :  ' ;    ;   \  ' . '  :  | \  \ ,'
        |  ; ' |  | '     \   \   ' |  |  '  '--'
        :  | : ;  ; |      \   `  ; |  :  :
        '  :  `--'   \      :   \ | |  | ,'
        :  ,      .-./       '---"  `--''
         `--`----'                               ,----,
                    ,--,                       ,/   .`|
  ,----..         ,--.'|    ,---,            ,`   .'  :
 /   /   \     ,--,  | :   '  .' \         ;    ;     /
|   :     : ,---.'|  : '  /  ;    '.     .'___,/    ,'
.   |  ;. / |   | : _' | :  :       \    |    :     |
.   ; /--`  :   : |.'  | :  |   /\   \   ;    |.';  ;
;   | ;     |   ' '  ; : |  :  ' ;.   :  `----'  |  |
|   : |     '   |  .'. | |  |  ;/  \   \     '   :  ;
.   | '___  |   | :  | ' '  :  | \  \ ,'     |   |  '
'   ; : .'| '   : |  : ; |  |  '  '--'       '   :  |
'   | '/  : |   | '  ,/  |  :  :             ;   |.'
|   :    /  ;   : ;--'   |  | ,'             '---'
 \   \ .'   |   ,/       `--''
  `---`     '---'
"""

print '\nHallo %s welkom op onze Chat Applicatie \n' % (user_name)

if ('lonneke' in user_name.lower()):
    print 'Welkom Mw. van der Velden! \n Wij hopen dat je onze universitaire chatapplicatie kan waarderen'

def prompt():
    sys.stdout.write(user_name + ' : ')
    sys.stdout.flush()


if __name__ == "__main__":
    if (len(sys.argv) < 3):
        print 'Gebruik a.u.b het correcte formaat : ./client.py hostname port'
        sys.exit()
    host = sys.argv[1]
    port = int(sys.argv[2])

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)

    # connect to remote host
    try:
        s.connect((host, port))
    except:
        print 'Niet mogelijk om te verbinden. Controleer de server!'
        sys.exit()

    print 'U bent verbonden met de server. U kan nu berichten versturen! \n'
    prompt()

    while 1:
        socket_list = [sys.stdin, s]

        # Ontvang een lijst van Sockets die leesbaar zijn
        read_sockets, write_sockets, error_sockets = select.select(socket_list, [], [])

        for sock in read_sockets:

            # Inkomende boodschap van server
            if sock == s:
                data = sock.recv(4096)
                if not data:
                    print '\n De verbinding met de chat server is verbroken'
                    sys.exit()
                else:
                    sys.stdout.write('\r')
                    sys.stdout.write(data)
                    prompt()

            # De gebruiker zijn boodschap
            else:
                msg = sys.stdin.readline()
                s.send(user_name + ' : ' + msg)
                prompt()
